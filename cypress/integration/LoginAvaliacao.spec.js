/// <reference types="cypress" />

import LoginAvaliacao from '../support/pages/LoginAvaliacao'

context('Login no módulo Avaliação', () => {
    it('Usuário e senha correto', () => {
        
        LoginAvaliacao.LoginUsuarioSenhaCorreto()
    })

    it('Usuário e senha incorreto', () => {
        LoginAvaliacao.LoginUsuarioSenhaIncorreto()
    })

    it('Login sem Senha', () => {
        LoginAvaliacao.LoginSemSenha()
    })

    it('Logoff com sucesso', () => {
        LoginAvaliacao.LogoffSucesso()
    })
   
    });
