/// <reference types="cypress" />

import AgendaAula from '../support/pages/AgendaAula'
import LoginTreino from '../support/pages/LoginTreino'

before(() => {
    LoginTreino.LoginUsuarioSenhaCorreto()
    cy.viewport(1366, 768) 
})


context('Agenda de Aulas', () => {
    it('Inserir aluno em uma aula e visualizar log', () => {
        AgendaAula.InserirAlunoemAula()
        AgendaAula.ExcluirAlunoemAula()
        AgendaAula.LogemAula()
    });

});