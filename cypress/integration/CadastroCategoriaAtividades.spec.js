/// <reference types="cypress" />

import CadastroCategoriaAtividades from '../support/pages/CadastroCategoriaAtividades'
import LoginTreino from '../support/pages/LoginTreino'

before(() => {
    LoginTreino.LoginUsuarioSenhaCorreto()
    cy.viewport(1366, 768) 
})


context('Cadastro de Categoria de Atividades - Treino', () => {
    it('Adicionar, Excluir e Log', () => {
        CadastroCategoriaAtividades.AdicionarCategoria()
        CadastroCategoriaAtividades.ExcluirCategoria()
        
    });

   
});