/// <reference types="cypress" />

import AulaFreepass from '../support/pages/AulaFreepass'
import LoginTreino from '../support/pages/LoginTreino'

before(() => {
    LoginTreino.LoginUsuarioSenhaCorreto()
    cy.viewport(1366, 768) 
})


context('Agenda de Aulas', () => {
    it('Inserir aluno frepass e visualizar log', () => {
        AulaFreepass.InserirAlunoemAula()
        AulaFreepass.ExcluirAlunoemAula()
        AulaFreepass.LogemAula()
    });

});