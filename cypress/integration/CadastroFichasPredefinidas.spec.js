/// <reference types="cypress" />

import CadastroFichasPredefinidas from '../support/pages/CadastroFichasPredefinidas'
import LoginTreino from '../support/pages/LoginTreino'

before(() => {
    LoginTreino.LoginUsuarioSenhaCorreto()
    cy.viewport(1366, 768) 
})


context('Cadastro de Fichas Predefinidas', () => {
    it('Inserir e Excluir', () => {
        CadastroFichasPredefinidas.CriarFicha()
        CadastroFichasPredefinidas.InativarFicha()
        
    });

   
});