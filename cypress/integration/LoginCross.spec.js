/// <reference types="cypress" />

import LoginCross from '../support/pages/LoginCross'


context('Login no módulo Cross', () => {
    it('Usuário e senha correto', () => {

        LoginCross.LoginUsuarioSenhaCorreto()
    })

    it('Usuário e senha incorreto', () => {
        LoginCross.LoginUsuarioSenhaIncorreto()
    })

    it('Login sem Senha', () => {
        LoginCross.LoginSemSenha()
    })

    it('Logoff com sucesso', () => {
        LoginCross.LogoffSucesso()
    })

});