/// <reference types="cypress" />

import LoginTreino from '../support/pages/LoginTreino'

context('Login no módulo Treino 3.0', () => {
    it('Usuário e senha correto', () => {
        
        LoginTreino.LoginUsuarioSenhaCorreto()
    })

    it('Usuário e senha incorreto', () => {
        LoginTreino.LoginUsuarioSenhaIncorreto()
    })

    it('Login sem Senha', () => {
        LoginTreino.LoginSemSenha()
    })

    it('Logoff com sucesso', () => {
        LoginTreino.LogoffSucesso()
    })
   
    });
