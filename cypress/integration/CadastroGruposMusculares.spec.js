/// <reference types="cypress" />

import CadastroGruposMusculares from '../support/pages/CadastroGruposMusculares'
import LoginTreino from '../support/pages/LoginTreino'

before(() => {
    LoginTreino.LoginUsuarioSenhaCorreto()
    cy.viewport(1366, 768) 
})


context('Cadastro de Grupos Musculares - Treino', () => {
    it('Adicionar, Excluir e Log', () => {
        CadastroGruposMusculares.AdicionarGrupoMuscular()
        CadastroGruposMusculares.ExcluirGrupoMuscular()
        
    });

   
});