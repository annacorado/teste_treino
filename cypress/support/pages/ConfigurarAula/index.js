/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovaAula {
    CriarAula() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Agenda).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ConfigurarAula).click()
        cy.get(el.AdicionarAula).click()
        cy.get(el.NomeAula).type('Teste Anna!!!')
        cy.get(el.Modalidade).select('4')
        cy.get(el.Professor).select('26')
        cy.get(el.Ambiente).select('1')
        cy.get(el.TorelanciaAte).type('2')
        cy.get(el.ToleranciaMinutos).select('1')
        cy.get(el.Capacidade).type('10')
        cy.get(el.Dias).check()
        cy.get(el.Datainicio).type('02/10/2025')
        cy.get(el.DataFinal).type('02/12/2026')
        cy.get(el.Horarios).type('08000900')
        cy.get(el.AdicionarHorarios).click()
        cy.get(el.BotaoSalvar).click()
        
        }
    ExcluirAula() {
        cy.get(el.BotaoExcluir).click()
        cy.get(el.ConfirmarExcluir).click()
        cy.get(el.Log).click()
        cy.get(el.FecharLog).click()

    }
 

    }

export default new NovaAula();