/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class DashAgenda {
    Dashboard() {
        cy.get(el.ExpandirMenu).click()
        cy.get(el.Agenda).click()
        cy.get(el.Atualizar).click()
        cy.get(el.Bonificacao).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.ValorBonificacao).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.Professores).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.TreinosNovos).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.TreinosRenovados).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.AvaliacaoFisica).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.Agendados).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.Executados).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.Faltas).click()
        cy.get(el.BotaoFechar).click()
        cy.get(el.Cancelados).click()
        cy.get(el.BotaoFechar).click()
        
        
         }
  
   
}
export default new DashAgenda();