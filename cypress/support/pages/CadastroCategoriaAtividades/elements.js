export const ELEMENTS = {
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: ':nth-child(3) > .menu-name',
    Aparelhos: ':nth-child(3) > .menu-name',
    AdicionarCategoria: '#adicionarCategoriaAtividades',
    NomeCategoria: '#nome-categoria-atividade-input',
    SalvarCategoria: '#btn-add-categoria-atividade',
    ExcluirCategoria: '#element-0-remove',
    Confirmar: '#action-remover',
    LogAparelho: '#show-log',
    FecharLog: '.close'

}