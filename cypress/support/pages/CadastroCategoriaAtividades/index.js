/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovaCategoria {
    AdicionarCategoria() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.Aparelhos).click()
        cy.get(el.AdicionarCategoria).click()
        cy.get(el.NomeCategoria).type('Anna - Teste automatizado')
        cy.get(el.SalvarCategoria).click()
        cy.get(el.LogAparelho).click()
        cy.get(el.FecharLog).click()
                   
        }
     ExcluirCategoria() {
        cy.get(el.ExcluirCategoria).click()
        cy.get(el.Confirmar).click()

}
 

 }

export default new NovaCategoria();