/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovoObjetivo {
    Adicionar() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Avaliacao).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Objetivos).click()
        cy.get(el.AdicionarObjetivo).click()
        cy.get(el.Nome).type('HIPERTROFIA 01')
        cy.get(el.Salvar).click()
        cy.get(el.Log).click()
        cy.get(el.FecharLog).click()
                                  
        }
     Excluir() {
        cy.get(el.Excluir).click()
        cy.get(el.Confirmar).click()

}
 

 }

export default new NovoObjetivo();