export const ELEMENTS = {
    Avaliacao: '[title="Avaliação Física"] > .img-aux > img',
    ExpandirMenuLateral: '.menu-toggle > .pct',
    Objetivos: ':nth-child(3) > .menu-name',
    AdicionarObjetivo: '#novo-objetivo',
    Nome: '#objetivo-input-nome',
    Salvar: '#objetivo-button-salvar',
    Excluir: '#element-0-remove',
    Confirmar: '#action-remover',
    Log: '#show-log',
    FecharLog: '.close'

}