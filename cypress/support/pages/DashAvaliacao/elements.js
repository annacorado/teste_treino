export const ELEMENTS = {
    ExpandirMenu: '.menu-toggle > .pct',
    BotaoAvaliacao: '[title="Avaliação Física"] > .img-aux > img',
    DashAvaliacao: '[href="/novotreino/pt/avaliacao/bi"] > .pct',
    AvaliacoesPeriodo: ':nth-child(1) > pacto-title-card > .card-wrapper > .content > .result-obtido-principal > .title-number',
    FecharModal: '#close-aba',
    AvaliacoesPeriodoNovos:':nth-child(1) > pacto-title-card > .card-wrapper > .content > .info-results > [style="border-right: 1px solid #dedede"] > .subtitle-number',
    AvaliacoesPeriodoReav: ':nth-child(1) > pacto-title-card > .card-wrapper > .content > .info-results > :nth-child(2) > .subtitle-number',
    PrevisaoReavilacao: ':nth-child(2) > pacto-title-card > .card-wrapper > .content > .result-obtido-principal > .title-number',
    ReavilacaoRealizadas: ':nth-child(2) > pacto-title-card > .card-wrapper > .content > .info-results > :nth-child(1) > .subtitle-number',
    ReavilacaoAtrasadas: ':nth-child(2) > pacto-title-card > .card-wrapper > .content > .info-results > :nth-child(2) > .subtitle-number',
    ReavilacaoFuturas: ':nth-child(3) > .subtitle-number',
    AlunosTotal: ':nth-child(3) > pacto-title-card > .card-wrapper > .content > .result-obtido-principal > .title-number',
    AtivosAvaliacao: ':nth-child(3) > pacto-title-card > .card-wrapper > .content > .info-results > .result-obtido > .subtitle-number',
    AlunosGoduraTotal: '.card-wrapper > :nth-child(1) > :nth-child(1) > :nth-child(3)',
    AlunosPesoTotal: '[style="border-bottom: 1px solid #dedede"] > .title-number',
    AlunosMassaMagraTotal: ':nth-child(2) > :nth-child(1) > .title-number',
    AlunosParQ: ':nth-child(2) > :nth-child(2) > .title-number'
  

}