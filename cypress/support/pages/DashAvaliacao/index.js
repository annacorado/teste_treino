/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class DashAvaliacao {
    Dashboard() {
        cy.get(el.ExpandirMenu).click()
        cy.get(el.BotaoAvaliacao).click()
        cy.get(el.AvaliacoesPeriodo).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AvaliacoesPeriodoNovos).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AvaliacoesPeriodoReav).click()
        cy.get(el.FecharModal).click()
        cy.get(el.PrevisaoReavilacao).click()
        cy.get(el.FecharModal).click()
        cy.get(el.ReavilacaoRealizadas).click()
        cy.get(el.FecharModal).click()
        cy.get(el.ReavilacaoAtrasadas).click()
        cy.get(el.FecharModal).click()
        cy.get(el.ReavilacaoFuturas).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AlunosTotal).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AtivosAvaliacao).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AlunosGoduraTotal).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AlunosPesoTotal).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AlunosMassaMagraTotal).click()
        cy.get(el.FecharModal).click()
        cy.get(el.AlunosParQ).click()
        cy.get(el.FecharModal).click()
        
        
         }
  
   
}
export default new DashAvaliacao();