export const ELEMENTS = {
    BotaoAvaliacao: '[title="Avaliação Física"] > .img-aux > img',
    ExpandirMenu: '.menu-toggle > .pct',
    Anamneses: 'a:nth-child(2) > .menu-name',
    Adicionar: '#btn-novo-anamnese',
    NomeAnamnese: '#nome-anamnese-input',
    AdicionarPergunta: '#btn-add-pergunta',
    Pergunta: '.anamnese-pergunta-wrapper',
    Descricao:'#descricao-pergunta-0',
    MaisPergunta: '#icon-add-pergunta-0',
    Pergunta01: '#descricao-pergunta-1',
    Tipo01: '#tipo-pergunta-select-1',
    Opcao01: '#add-opcao-check-1',
    MaisPergunta02: '#icon-add-pergunta-1',
    Pergunta02: '#descricao-pergunta-2',
    Tipo02: '#tipo-pergunta-select-2',
    Opcao02: '#add-opcao-check-2',
    MaisPergunta03: '#icon-add-pergunta-2',
    Pergunta03: '#descricao-pergunta-3',
    Tipo03: '#tipo-pergunta-select-3',
    CriarAnamnese: '#btn-add-anamnese',
    ExcluirAnamnese: '#element-7-remove',
    ConfirmarExclusao: '#action-remover',
    Log: '#show-log',
    FecharLog: '.close'





} 