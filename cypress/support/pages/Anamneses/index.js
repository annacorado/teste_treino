/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class Anamneses {
    Adicionar() {
        cy.get(el.ExpandirMenu).click()
        cy.get(el.BotaoAvaliacao).click()
        cy.get(el.ExpandirMenu).click()
        cy.get(el.Anamneses).click()
        cy.get(el.Adicionar).click()
        cy.get(el.NomeAnamnese).type("Teste automatizado - Anna")
        cy.get(el.AdicionarPergunta).click()
        cy.get(el.Pergunta).click()
        cy.get(el.Descricao).type("Você já fez isso?")
        cy.get(el.MaisPergunta).click()
        cy.get(el.Pergunta01).type("Será mesmo?")
        cy.get(el.Tipo01).select("ESCOLHA_MULTIPLA")
        cy.get(el.Opcao01).click()
        cy.get(el.Opcao01).click()
        cy.get(el.Opcao01).click()
        cy.get(el.MaisPergunta02).click()
        cy.get(el.Pergunta02).type("Que tal isso?")
        cy.get(el.Tipo02).select("ESCOLHA_UNICA")
        cy.get(el.Opcao02).click()
        cy.get(el.Opcao02).click()
        cy.get(el.MaisPergunta03).click()
        cy.get(el.Pergunta03).type("Isso ai mesmo, vai na fé")
        cy.get(el.Tipo03).select("SIM_NAO")
        cy.get(el.CriarAnamnese).click()
        
         }
  
    Excluir(){
        cy.get(el.ExcluirAnamnese).click()
        cy.get(el.ConfirmarExclusao).click()
        cy.get(el.Log).click()
        cy.get(el.FecharLog).click()


    }
   
}
export default new Anamneses();