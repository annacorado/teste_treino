/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovoNivel {
    AdicionarNivel() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.Niveis).click()
        cy.get(el.AdicionarNivel).click()
        cy.get(el.NomeNivel).type('Anna - Teste automatizado')
        cy.get(el.OrdemNivel).type('10')
        cy.get(el.SalvarNivel).click()
        cy.get(el.LogAparelho).click()
        cy.get(el.FecharLog).click()
                   
        }
     ExcluirInativarNivel() {
         cy.get(el.InativarNivel).click()
         cy.get(el.ConfirmarInativado).click()
        cy.get(el.ExcluirNivel).click()
        cy.get(el.Confirmar).click()

        }

}


export default new NovoNivel();