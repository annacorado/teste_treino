export const ELEMENTS = {
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: ':nth-child(3) > .menu-name',
    Niveis: ':nth-child(8) > .menu-name',
    AdicionarNivel: '#adicionarNivel',
    NomeNivel: '#input-nome-nivel',
    OrdemNivel: '#input-ordem-nivel',
    SalvarNivel: '#gravarCadastroNivel',
    InativarNivel: '#element-0-desativar',
    ConfirmarInativado: '#action-desativar',
    ExcluirNivel: '#element-0-remove',
    Confirmar: '#action-remover',
    LogAparelho: '#show-log',
    FecharLog: '.close'

}