export const ELEMENTS = {
        ExpandirMenu: '.menu-toggle > .pct',
        Pessoas: 'i.pct.pct-users',
        PrimeiroCliente: '#element-0 > :nth-child(2)',
        RemoverPrograma: '#programa-0-remove',
        ConfirmarRemover: '#action-remover',
        RenovarPrograma: '#renovar > .content',
        RenovarAtual: '#button-renovar-atual-programa',
        NovoPrograma: '#button-criar-novo-programa',
        VoltarPerfilAluno: '.card-info-programa > .pct-arrow-left',
        NomedoPrograma: '#input-nome-programa',
        SalvarPrograma: '#btn-salvar-programa > .content',
        SelecionarAtividade: '#select-atividade > .current-wrapper > .option-label',
        PesquisarAtividade: '#select-atividade-filter',
        Atividade01: '#select-atividade-0 > .option-container > span',
        Atividade02: '#select-atividade-1 > .option-container > span',
        Atividade03: '#select-atividade-2 > .option-container > span',
        Series: '#input-series',
        Repeticoes: '#input-repticoes',
        Carga: '#input-peso',
        Cadencia: '#input-cadencia',
        Descanso: '#input-descanso',
        AdicionarAtividade: '#btn-adcionar',
        SalvarFicha: 'i.pct.pct-pct.pct-check.ng-star-inserted',
        ReplicarSeries: 'i.pct.pct-pct.pct-copy.ng-star-inserted',
        PadraoSerie: ':nth-child(1) > .aux-wrapper > .ng-pristine',
        PadraoRepeticoes:'.block-input > :nth-child(2) > .aux-wrapper > .ng-untouched',
        PadraoCarga:'.block-input > :nth-child(3) > .aux-wrapper > .ng-pristine',
        PadraoCadencia:'.block-input > :nth-child(4) > .aux-wrapper > .ng-untouched',
        PadraoDescanso:'.block-input > :nth-child(5) > .aux-wrapper > .ng-pristine',
        DefinirPadrao: 'pacto-cat-button.botao-padrao.definir',
        
  
}



