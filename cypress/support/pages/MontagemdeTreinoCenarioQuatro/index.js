/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class MontagemTreinoNovo {
    NovaFichaAtividades() {
        cy.get(el.ExpandirMenu).click()
        cy.get(el.Pessoas).click()
        cy.get(el.PrimeiroCliente).click()
        cy.get(el.RemoverPrograma).click()
        cy.get(el.ConfirmarRemover).click()
        cy.get(el.RenovarPrograma).click()
        cy.get(el.NovoPrograma).click()
        cy.get(el.ExcluirNomePrograma).click()
        cy.get(el.NomedoPrograma).type('Cenario quatro')
        cy.get(el.SalvarPrograma).click()
        cy.get(el.NovaFicha).click()
        cy.get(el.CriarNovaFicha).click()
        cy.get(el.VariasAtividades).click()
        cy.get(el.Atividade01).click()
        cy.get(el.Atividade02).click()
        cy.get(el.Atividade03).click()
        cy.get(el.FecharAtividade).click()
        cy.get(el.VoltarPerfilAluno).click()
        
        
         }


}
export default new MontagemTreinoNovo();