export const ELEMENTS = {
    ExpandirMenu: '.menu-toggle > .pct',
    BotaoCross: '[title="Cross"] > .img-aux > img',
    AlunosAtivos: ':nth-child(1) > pacto-small-info-card > .small-info-card-wrapper > .left-side',
    Resultados: '.col-md-7 > .row > :nth-child(2) > pacto-small-info-card > .small-info-card-wrapper > .right-side > .valor',
    MarcacaoemAula: '.col-md-5 > pacto-small-info-card > .small-info-card-wrapper',
    BotaoFechar: 'button.close.modal-item.close-modal'
  

}