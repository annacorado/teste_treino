export const ELEMENTS = {
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: ':nth-child(3) > .menu-name',
    Aparelhos: '[href="/pt/treino/cadastros/aparelhos"]',
    AdicionarAparelho: '#btn-novo-aparelho',
    NomeAparelho: '#nome-aparelho-input',
    Ajustes: '.input-row > .form-group > .form-control',
    AdicionarAjustes: '.input-row > .btn',
    SalvarAparelho: '#btn-add-aparelho',
    Cancelar: '.actions > .btn-secondary',
    ExcluirAparelho: '#element-2-remove',
    Confirmar: '#action-remover',
    LogAparelho: '#show-log',
    FecharLog: '.close'

}