/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class MontagemTreinoNovo {
    NovoTreino() {
        cy.get(el.ExpandirMenu).click()
        cy.get(el.Pessoas).click()
        cy.get(el.PrimeiroCliente).click()
        cy.get(el.RemoverPrograma).click()
        cy.get(el.ConfirmarRemover).click()
        cy.get(el.RenovarPrograma).click()
        cy.get(el.NovoPrograma).click()
        cy.get(el.NomedoPrograma).type('Teste automatizado')
        cy.get(el.SalvarPrograma).click()
        cy.get(el.VariasAtividades).click()
        cy.get(el.Atividade01).click()
        cy.get(el.Atividade02).click()
        cy.get(el.Atividade03).click()
        cy.get(el.RemoverAtividade).click()
        cy.get(el.FecharAtividade).click()
        cy.get(el.ReplicarSeries).click()
        cy.get(el.PadraoSerie).type('6')
        cy.get(el.PadraoRepeticoes).type('6')
        cy.get(el.PadraoCarga).type('6')
        cy.get(el.PadraoCadencia).type('6')
        cy.get(el.PadraoDescanso).type('0006')
        cy.get(el.DefinirPadrao).click()
        cy.get(el.SalvarFicha).click()
        cy.get(el.ExcluirFicha).click()
        cy.get(el.ConfirmarExclusao).click()
        cy.get(el.VoltarPerfilAluno).click()
        
        
         }


}
export default new MontagemTreinoNovo();