export const ELEMENTS = {
    ExpandirMenu: '.menu-toggle > .pct',
    Pessoas: 'i.pct.pct-users',
    PrimeiroCliente: '#element-0 > :nth-child(2)',
    RemoverPrograma: '#programa-0-remove',
    ConfirmarRemover: '#action-remover',
    RenovarPrograma: '#renovar > .content',
    RenovarAtual: '#button-renovar-atual-programa',
    NovoPrograma: '#button-criar-novo-programa',
    VoltarPerfilAluno: '.card-info-programa > .pct-arrow-left',
    NomedoPrograma: '#input-nome-programa',
    SalvarPrograma: '#btn-salvar-programa > .content',
    VariasAtividades: 'i.pct.pct-pct.pct-layers.ng-star-inserted',
    ProgramaAtual: '#programa-0',
    Atividade01: ':nth-child(2) > .block-actions > .btn-add',
    Atividade02: ':nth-child(3) > .block-actions > .btn-add',
    Atividade03: ':nth-child(4) > .block-actions > .btn-add',
    Atividade04: ':nth-child(5) > .block-actions > .btn-add',
    RemoverAtividade: ':nth-child(4) > .block-actions > .btn-remove',
    FecharAtividade: '.text-right > .pct',
    ReplicarSeries: 'i.pct.pct-pct.pct-copy.ng-star-inserted',
    PadraoSerie: ':nth-child(1) > .aux-wrapper > .ng-pristine',
    PadraoRepeticoes:'.block-input > :nth-child(2) > .aux-wrapper > .ng-untouched',
    PadraoCarga:'.block-input > :nth-child(3) > .aux-wrapper > .ng-pristine',
    PadraoCadencia:'.block-input > :nth-child(4) > .aux-wrapper > .ng-pristine',
    PadraoDescanso:'.block-input > :nth-child(5) > .aux-wrapper > .ng-pristine',
    DefinirPadrao: 'pacto-cat-button.botao-padrao.definir',
    SalvarFicha: 'i.pct.pct-pct.pct-check.ng-star-inserted',
    ExcluirFicha: 'i.pct.pct-pct.pct-trash-2.ng-star-inserted',
    ConfirmarExclusao: '#action-ok'












}