export const ELEMENTS = {
    Cross: '[title="Cross"] > .img-aux > img',
    ExpandirMenuLateral: '.menu-toggle > .pct',
    Cadastros: ':nth-child(4) > .menu-name',
    Benchmarks: ':nth-child(3) > .menu-name',
    AdicionarBenchmark: '#btn-novo-benchmark',
    NomeBenchmark: '#nome-benchmark-input',
    Tipo: '#tipo-benchmark-select',
    Exercicios: '#exercicios-benchmark-text-area',
    Observacao: '#observacao-benchmark-text-area',
    SalvarBenchmark: '#btn-add-benchmark',
    Cancelar: '.btn-secondary',
    Excluir: '#element-1-remove',
    Confirmar: '#action-remover',
    Log: '#show-log',
    FecharLog: '.close'

}