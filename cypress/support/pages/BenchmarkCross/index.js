/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovoBenchmark {
    AdicionarBenchmark() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cross).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.Benchmarks).click()
        cy.get(el.AdicionarBenchmark).click()
        cy.get(el.NomeBenchmark).type('ABBATE - ANNA')
        cy.get(el.Tipo).select("1")
        cy.get(el.Exercicios).type('teste')
        cy.get(el.Observacao).type('teste teste do teste')
        cy.get(el.SalvarBenchmark).click()
                                  
        }
     ExcluirBenchmark() {
        cy.get(el.Excluir).click()
        cy.get(el.Confirmar).click()
        cy.get(el.Log).click()
        cy.get(el.FecharLog).click()

}
 

 }

export default new NovoBenchmark();