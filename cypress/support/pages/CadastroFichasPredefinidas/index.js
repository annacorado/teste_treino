/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovaFicha {
    CriarFicha() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.FichasPredefinidas).click()
        cy.get(el.AdicionarFicha).click()
        cy.get(el.NomeFicha).type('Anna - Teste automatizado')
        cy.get(el.CategoriaFicha).select("1")
        cy.get(el.MensagemAluno).type('Bem vindo, aluno!')
        cy.get(el.VariasAtividades).click()
        cy.get(el.Atividade01).click()
        cy.get(el.Atividade02).click()
        cy.get(el.Atividade03).click()
        cy.get(el.FecharAtividade).click()
        cy.get(el.VoltarListagem).click()
             
        }
     InativarFicha() {
        cy.get(el.InativarFicha).click()
        cy.get(el.Confirmar).click()
        cy.get(el.Log).click()
        cy.get(el.FecharLog).click()

}
 

 }

export default new NovaFicha();