export const ELEMENTS = {
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: ':nth-child(3) > .menu-name',
    FichasPredefinidas: ':nth-child(6) > .menu-name',
    AdicionarFicha: '#btn-nova-ficha-predefinida',
    NomeFicha: '#nome-ficha-input',
    CategoriaFicha: '#categoria-select',
    MensagemAluno: '#mensagem-aluno-input',
    VariasAtividades: 'i.pct.pct-pct.pct-layers.ng-star-inserted',
    ProgramaAtual: '#programa-0',
    Atividade01: ':nth-child(2) > .block-actions > .btn-add',
    Atividade02: ':nth-child(3) > .block-actions > .btn-add',
    Atividade03: ':nth-child(4) > .block-actions > .btn-add',
    Atividade04: ':nth-child(5) > .block-actions > .btn-add',
    FecharAtividade: '.text-right > .pct',
    VoltarListagem: '.lower-menu > :nth-child(1) > .pct',
    InativarFicha: '#element-0-situacao',
    Confirmar: '#action-inativar',
    Log: '#show-log',
    FecharLog: '.close'

}