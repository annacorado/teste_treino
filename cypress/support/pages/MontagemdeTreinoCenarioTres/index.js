/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class MontagemdeTreino {
    AdicionarAlterarMetodo() {
        cy.get(el.ExpandirMenu).click()
        cy.get(el.Pessoas).click()
        cy.get(el.PrimeiroCliente).click()
        cy.get(el.RemoverPrograma).click()
        cy.get(el.ConfirmarRemover).click()
        cy.get(el.RenovarPrograma).click()
        cy.get(el.NovoPrograma).click()
        cy.get(el.NomedoPrograma).type('Cenario Tres')
        cy.get(el.SalvarPrograma).click()
        cy.get(el.SelecionarAtividade).click()
        cy.get(el.PesquisarAtividade).type('Abdu')
        cy.get(el.Atividade01).click()
        cy.get(el.Series).type('5')
        cy.get(el.Repeticoes).type('4')
        cy.get(el.Descanso).type('0007')
        cy.get(el.AdicionarAtividade).click()
        cy.get(el.SelecionarAtividade).click()
        cy.get(el.PesquisarAtividade).type('Abdu')
        cy.get(el.Atividade02).click()
        cy.get(el.AdicionarAtividade).click()
        cy.get(el.SelecionarAtividade).click()
        cy.get(el.PesquisarAtividade).type('Abdu')
        cy.get(el.Atividade03).click()
        cy.get(el.AdicionarAtividade).click()
        cy.get(el.SelecionarAtividade).click()
        cy.get(el.PesquisarAtividade).type('Abdom')
        cy.get(el.Atividade04).click()
        cy.get(el.Limpar).click()
        cy.get(el.Series).type('20')
        cy.get(el.AdicionarAtividade).click()
        cy.get(el.SalvarFicha).click()
        cy.get(el.VoltarPerfilAluno).click()

        
    }

}
export default new MontagemdeTreino();