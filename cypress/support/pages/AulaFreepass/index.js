/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class Freepass {
    InserirAlunoemAula() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Agenda).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.AulaMenuLateral).click()
        cy.get(el.AulaAgenda).click()
        cy.get(el.InserirAuno).click()
        cy.get(el.SelecionarAluno).click()
        cy.get(el.SalvarFrepass).click()
        cy.contains('Aluno inserido com sucesso')
        
        
         }
    ExcluirAlunoemAula() {
        cy.get(el.ExcluirAluno).click()
        cy.get(el.BotãoConfirmarExclusão).click()

    }
    SubstituirProfessor(){
        cy.get(el.SubstituirProfessor).click()
        cy.get(el.SelecionarProfessor).click()
        cy.get(el.JustificativaProfessor).type('teste')
        cy.get(el.SalvarProfessorSubstituto).click

    }
    LogemAula(){
        cy.get(el.BotaoLog).click()
        cy.get(el.FecharLog).click()


    }
}
export default new Freepass();