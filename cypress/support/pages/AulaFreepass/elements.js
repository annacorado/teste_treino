export const ELEMENTS = {
    Agenda: '[href="/pt/agenda"] > .pct',
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    AulaMenuLateral: '[href="/pt/agenda/painel/turmas"]',
    AulaAgenda: ' pacto-time-grid-container > .aula-TESTE > .wrapper',
    InserirAuno: '#aluno-select',
    ConfirmarAluno: 'pacto-turma-aula-experimental-modal.ng-star-inserted > :nth-child(3)',
    SelecionarAluno: '#aluno-select-10> .option-container > ',
    SalvarFrepass: '.content',
    BotaoLog: '#show-log',
    FecharLog: '.close > span',
    ExcluirAluno: '#acao-desmarcar-aluno-0',
    BotãoConfirmarExclusão: 'pacto-turma-desmarcar-modal > :nth-child(3)',


}