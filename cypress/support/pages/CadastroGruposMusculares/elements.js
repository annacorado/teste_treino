export const ELEMENTS = {
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: '.container-mask > :nth-child(3)',
    GruposMusculares: ':nth-child(5) > .menu-name',
    AdicionarGrupo: '#adicionarGruposMusculares',
    NomeGrupo: '#nome-grupo-muscular-input',
    SalvarGrupo: '#btn-add-grupo-muscular',
    ExcluirGrupo: '#element-1-remove',
    Confirmar: '#action-remover',
    LogAparelho: '#show-log',
    FecharLog: '.close'

}