/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovoGrupo {
    AdicionarGrupoMuscular() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.GruposMusculares).click()
        cy.get(el.AdicionarGrupo).click()
        cy.get(el.NomeGrupo).type('ABDOMEN - Teste automatizado')
        cy.get(el.SalvarGrupo).click()
        cy.get(el.LogAparelho).click()
        cy.get(el.FecharLog).click()
                   
        }
     ExcluirGrupoMuscular() {
        cy.get(el.ExcluirGrupo).click()
        cy.get(el.Confirmar).click()

}
 

 }

export default new NovoGrupo();