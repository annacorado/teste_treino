/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovoAparelho {
    AdicionarAparelho() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cross).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.Aparelhos).click()
        cy.get(el.ExcluirAparelho).click()
        cy.get(el.Confirmar).click()
        cy.get(el.AdicionarAparelho).click()
        cy.get(el.NomeAparelho).type('Anna - Teste automatizado')
        cy.get(el.Ajustes).type('teste')
        cy.get(el.AdicionarAjustes).click()
        cy.get(el.SalvarAparelho).click()
        cy.get(el.LogAparelho).click()
        cy.get(el.FecharLog).click()
                   
        }
     ExcluirAparelho() {
        cy.get(el.ExcluirAparelho).click()
        cy.get(el.Confirmar).click()

}
 

 }

export default new NovoAparelho();