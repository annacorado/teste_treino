export const ELEMENTS = {
    Cross: '[title="Cross"] > .img-aux > img',
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: ':nth-child(4) > .menu-name',
    Aparelhos: ':nth-child(1) > .menu-name',
    AdicionarAparelho: '#btn-novo-aparelho',
    NomeAparelho: '#nome-aparelho-input',
    Ajustes: '.input-row > .form-group > .form-control',
    AdicionarAjustes: '.input-row > .btn',
    SalvarAparelho: '#btn-add-aparelho',
    ExcluirAparelho: '#element-0-remove',
    Confirmar: '#action-remover',
    LogAparelho: '#show-log',
    FecharLog: '.close'

}