export const ELEMENTS = {
    Usuario:  '#fmLay\\:usernameLoginZW',
    Senha: '#fmLay\\:pwdLoginZW',
    BotaoEntrar: '#fmLay\\:btnEntrar',
    BotaoModuloCross: '#loginModuloCrossFit > a > .lgmodule > img', 
    JanelaUsuario: '.user-section > .pct',
    BotaoLogoff: '.button-log-out'
 }