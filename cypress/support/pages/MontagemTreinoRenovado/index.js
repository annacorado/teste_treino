/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class MontagemTreinoRenovado {
    RenovarAtual(){
        cy.get(el.ExpandirMenu).click()
        cy.get(el.Pessoas).click()
        cy.get(el.PrimeiroCliente).click()
        cy.get(el.RenovarPrograma).click()
        cy.get(el.RenovarAtual).click()
        cy.get(el.ExcluirFicha).click()
        cy.get(el.ConfirmarExclusao).click()
        cy.get(el.NomeAtividade).type('Abdominal')
        cy.get(el.SelecionarAtividade).click()
        cy.get(el.Series).type('5')
        cy.get(el.Repeticoes).type('5')
        cy.get(el.Carga).type('5')
        cy.get(el.Cadencia).type('5')
        cy.get(el.Descanso).type('0005')
        cy.get(el.AdicionarAtividade).click()
        cy.get(el.VoltarPerfilAluno).click()
        cy.get(el.RemoverPrograma).click()
        cy.get(el.ConfirmarRemover).click()

    }
   
}
export default new MontagemTreinoRenovado();