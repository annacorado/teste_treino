export const ELEMENTS = {
    ExpandirMenu: '.menu-toggle > .pct',
    Pessoas: 'i.pct.pct-users',
    PrimeiroCliente: '#element-0 > :nth-child(2)',
    RemoverPrograma: '#programa-0-remove',
    ConfirmarRemover: '#action-remover',
    RenovarPrograma: '#renovar > .content',
    RenovarAtual: '#button-renovar-atual-programa',
    ExcluirFicha: 'i.pct.pct-pct.pct-trash-2.ng-star-inserted',
    ConfirmarExclusao: '#action-ok',
    NomeAtividade: '#select-atividade > .current-wrapper > .option-label',
    SelecionarAtividade: '#select-atividade-0 > .option-container > span',
    Series: '#input-series',
    Repeticoes: '#input-repticoes',
    Carga: '#input-peso',
    Cadencia: '#input-cadencia',
    Descanso: '#input-descanso',
    AdicionarAtividade: '#btn-adcionar > .content',
    VoltarPerfilAluno: '.card-info-programa > .pct-arrow-left',
   
  
  

   
    



}