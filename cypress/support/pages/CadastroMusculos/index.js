/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovoMusculo {
    AdicionarMusculo() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.Musculos).click()
        cy.get(el.AdicionarMusculo).click()
        cy.get(el.NomeMusculo).type('Anna - Teste automatizado')
        cy.get(el.SalvarMusculo).click()
        cy.get(el.LogAparelho).click()
        cy.get(el.FecharLog).click()
                   
        }
     ExcluirMusculo() {
        cy.get(el.ExcluirMusculo).click()
        cy.get(el.Confirmar).click()

}
 

 }

export default new NovoMusculo();