export const ELEMENTS = {
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: ':nth-child(3) > .menu-name',
    Musculos: ':nth-child(4) > .menu-name',
    AdicionarMusculo: '#adicionarMusculos',
    NomeMusculo: '#nome-musculo-input',
    SalvarMusculo: '#btn-add-musculo',
    ExcluirMusculo: '#element-0-remove',
    Confirmar: '#action-remover',
    LogAparelho: '#show-log',
    FecharLog: '.close'

}