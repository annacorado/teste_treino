export const ELEMENTS = {
    Cross: '[title="Cross"] > .img-aux > img',
    ExpandirMenuLateral: '.menu-toggle > .pct',
    Cadastros: ':nth-child(4) > .menu-name',
    TiposdeWOD: ':nth-child(5) > .menu-name',
    AdicionarTipodeWOD: '#btn-novo-tipo-wod',
    Nome: '.form-control',
    SalvarTipodeWOD: '.btn-primary',
    Excluir: '#element-4-remove',
    Confirmar: '#action-remover',
    Log: '#show-log',
    FecharLog: '.close'

}