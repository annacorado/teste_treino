/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovaAtividade {
    AdicionarAtividade() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.Atividades).click()
        cy.get(el.AdicionarAtividade).click()
        cy.get(el.NomeAtividade).type('ABDOMINAL (SOLO) 02')
        cy.get(el.Descricao).type('Teste teste teste teste')
        cy.get(el.TipoAtividade).select("NEUROMUSCULAR")
        cy.get(el.SalvarAtividade).click()
        cy.get(el.CancelarAtividade).click()
        cy.get(el.LogAparelho).click()
        cy.get(el.FecharLog).click()
                   
        }
  
 

 }

export default new NovaAtividade();