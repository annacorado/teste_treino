export const ELEMENTS = {
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    Cadastros: ':nth-child(3) > .menu-name',
    Atividades: ':nth-child(2) > .menu-name',
    AdicionarAtividade: '#addAtividade',
    NomeAtividade: '#inptNomeAtividade',
    Descricao: 'pacto-textarea > .form-group > .form-control',
    TipoAtividade: '#selectTipoAtividade',
    SalvarAtividade: '#btnSalvar',
    CancelarAtividade: '.actions > .btn-secondary',
    Confirmar: '#action-inativar',
    LogAparelho: '#show-log',
    FecharLog: '.close',
    }