/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class CriarAvaliacaoFisica {
    Avaliacao() {
        cy.get(el.Pessoas).click()
        cy.get(el.PrimeiroCliente).click()
        cy.get(el.RemoverAvaliacao).click()
        cy.get(el.ConfirmarExclusao).click()
        cy.get(el.CriarAvaliacao).click()
        cy.get(el.Hipertrofia).click()
        cy.get(el.Resposta01).type("Isso ai mesmo!")
        cy.get(el.Observacao01).type("Isso ai mesmo só que 2!")
        cy.get(el.Resposta02).type("Testando")
        cy.get(el.Observacao02).type("Testando de novo")
        cy.get(el.SalvarAnamnese).click()
        cy.get(el.PesoAltura).click()
        cy.get(el.Peso).type("80")
        cy.get(el.Altura).type("1,90")
        cy.get(el.SalvarPesoAltura).click()
        cy.get(el.Dobras).click()
        cy.get(el.Adbominal).type("40")
        cy.get(el.Peitoral).type("60")
        cy.get(el.CoxaMedial).type("33")
        cy.get(el.Panturrilha).type("39")
        cy.get(el.SalvarDobras).click()
        cy.get(el.ResultadoAvaliacao).click()
        cy.get(el.Fechar).click()
        cy.get(el.Opcoes).click()
        cy.get(el.VerHistorico).click()
        cy.get(el.FecharHistorico).click()
        cy.get(el.VoltarPerfil).click()
        
         }


}
export default new CriarAvaliacaoFisica();