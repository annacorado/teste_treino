export const ELEMENTS = {
    Agenda: '[href="/pt/agenda"] > .pct',
    ExpandirMenuLateral: '.menu-toggle > .pct ',
    AulaMenuLateral: '[href="/pt/agenda/painel/turmas"]',
    AulaAgenda: ' pacto-time-grid-container > .aula-TESTE > .wrapper',
    InserirAluno: '#aluno-select',
    ConfirmarAluno: 'pacto-turma-aula-experimental-modal.ng-star-inserted > :nth-child(3)',
    SelecionarAlunoTeste: '#aluno-select-0 > .option-container > ',
    BotãoConfirmar: 'pacto-turma-aula-experimental-modal > :nth-child(3)',
    BotaoLog: '#show-log',
    FecharLog: '.close > span',
    ExcluirAluno: '#acao-desmarcar-aluno-0',
    BotãoConfirmarExclusão: 'pacto-turma-desmarcar-modal > :nth-child(3)',


}