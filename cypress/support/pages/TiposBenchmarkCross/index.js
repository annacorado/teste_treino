/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovoTipodeBenchmark {
    Adicionar() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cross).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.TiposdeBenchmarks).click()
        cy.get(el.AdicionarTipodeBenchmark).click()
        cy.get(el.Nome).type('ANNA - Teste automatizado')
        cy.get(el.SalvarTipodeBenchmark).click()
        cy.get(el.Log).click()
        cy.get(el.FecharLog).click()
                                  
        }
     Excluir() {
        cy.get(el.Excluir).click()
        cy.get(el.Confirmar).click()

}
 

 }

export default new NovoTipodeBenchmark();