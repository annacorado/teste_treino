export const ELEMENTS = {
    Cross: '[title="Cross"] > .img-aux > img',
    ExpandirMenuLateral: '.menu-toggle > .pct',
    Cadastros: ':nth-child(4) > .menu-name',
    TiposdeBenchmarks: ':nth-child(4) > .menu-name',
    AdicionarTipodeBenchmark: '#adicionarTipoBenchmark',
    Nome: '.form-group > .form-control',
    SalvarTipodeBenchmark: '.modal-footer > .btn-primary',
    Cancelar: '.btn-secondary',
    Excluir: '#element-0-remove',
    Confirmar: '#action-remover',
    Log: '#show-log',
    FecharLog: '.close'

}