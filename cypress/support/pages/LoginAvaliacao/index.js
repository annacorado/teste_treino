/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

class LoginAvaliacao{
    LoginUsuarioSenhaCorreto(){
        cy.visit('/384acd1173d8511f56c4b26a337499e6')
        cy.get(el.Usuario).type('pactobr')
        cy.get(el.Senha).type('123')
        cy.get(el.BotaoEntrar).click()
        cy.get(el.BotaoModuloAvaliacao).click()
        
}

    LoginUsuarioSenhaIncorreto(){
        cy.visit('/384acd1173d8511f56c4b26a337499e6')
        cy.get(el.Usuario).type('pactobr')
        cy.get(el.Senha).type('124')
        cy.get(el.BotaoEntrar).click()
        cy.contains('Usuário ou senha inválidos!')
    }

    LoginSemSenha(){
        cy.visit('/384acd1173d8511f56c4b26a337499e6')
        cy.get(el.Usuario).type('pactobr')
        cy.get(el.BotaoEntrar).click()
        cy.contains('Preencha todos os campos para fazer o login!')
    }

    
    LogoffSucesso(){

        cy.visit('/384acd1173d8511f56c4b26a337499e6')
        cy.get(el.Usuario).clear().type('pactobr')
        cy.get(el.Senha).type('123')
        cy.get(el.BotaoEntrar).click()
        cy.get(el.BotaoModuloAvaliacao).click()
        cy.get(el.JanelaUsuario).click()
        cy.get(el.BotaoLogoff).click()
    }
}

export default new LoginAvaliacao();