export const ELEMENTS = {
    Usuario:  '#fmLay\\:usernameLoginZW',
    Senha: '#fmLay\\:pwdLoginZW',
    BotaoEntrar: '#fmLay\\:btnEntrar',
    BotaoModuloAvaliacao: '#loginModuloAvaliacaoFisica', 
    JanelaUsuario: '.user-section > .pct',
    BotaoLogoff: '.button-log-out'
 }