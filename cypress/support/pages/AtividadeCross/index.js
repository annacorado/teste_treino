/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class NovaAtividade {
    AdicionarAtividade() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cross).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.Cadastros).click()
        cy.get(el.Atividades).click()
        cy.get(el.InativarAtividade).click()
        cy.get(el.Confirmar).click()
        cy.get(el.AdicionarAtividade).click()
        cy.get(el.NomeAtividade).type('Anna - Teste automatizado')
        cy.get(el.Descricao).type('teste')
        cy.get(el.LinkYoutube).type('https://www.youtube.com/watch?v=3qhtsKehnh4')
        cy.get(el.SalvarAtividade).click()
        cy.get(el.Cancelar).click()
        cy.get(el.LogAtividade).click()
        cy.get(el.FecharLog).click()
                   
        }
     InativarAtividade() {
        cy.get(el.InativarAtividade).click()
        cy.get(el.Confirmar).click()

}
 

 }

export default new NovaAtividade();