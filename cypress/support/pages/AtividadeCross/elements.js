export const ELEMENTS = {
    Cross: '[title="Cross"] > .img-aux > img',
    ExpandirMenuLateral: '.menu-toggle > .pct',
    Cadastros: ':nth-child(4) > .menu-name',
    Atividades: ':nth-child(2) > .menu-name',
    AdicionarAtividade: '#btn-novo-atividade-crossfit',
    NomeAtividade: '#nome-atividade-crossfit-input',
    Descricao: '#descricao-atividade-text-area',
    LinkYoutube: '#link-youtube-atividade-input',
    SalvarAtividade: '#btn-add-atividade-crossfit',
    Cancelar: '.btn-secondary',
    InativarAtividade: '#element-2-remove',
    Confirmar: '#action-inativar',
    LogAtividade: '#show-log',
    FecharLog: '.close'

}