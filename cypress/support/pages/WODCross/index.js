/// <reference types="cypress" />

const el = require('./elements').ELEMENTS

import LoginTreino from '../LoginTreino'


class WODCross {
    NovoWOD() {
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.BotaoCross).click()
        cy.get(el.ExpandirMenuLateral).click()
        cy.get(el.CadastroWOD).click()
        cy.get(el.Adicionar).click()
        cy.get(el.Objetivo).type('Teste da Anna!')
        cy.get(el.NomedoObjetivo).select('1')
        cy.get(el.Aquecimento).type('Teste')
        cy.get(el.Alongamento).type('Teste')
        cy.get(el.Skill).type('Teste')
        cy.get(el.Complex).type('Teste')
        cy.get(el.WOD).type('WOD teste')
        cy.get(el.Salvar).click()
        cy.get(el.Remover).click()
        cy.get(el.ConfirmarExclusao).click()
        
        
         }


}
export default new WODCross();