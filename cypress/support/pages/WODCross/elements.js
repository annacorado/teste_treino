export const ELEMENTS = {
        BotaoCross:'[title="Cross"] > .img-aux > img',
        ExpandirMenuLateral: '.menu-toggle > .pct ',
        CadastroWOD: 'a:nth-child(2) > .menu-name',
        Adicionar: '#btn-add-wod.btn.btn-primary.novo-botao.ng-star-inserted',
        Objetivo: '#nome-wod-input',
        NomedoObjetivo: '#tipo-wod-select',
        Aquecimento: '#text-Aquecimento',
        Alongamento: '#text-Along-Mobil',
        Skill: '#textParteTecSkill',
        Complex: '#textComplexEmom',
        WOD: '#textWod',
        Salvar: '#btnSalvar',
        Remover: '#element-0-remove',
        ConfirmarExclusao: '#action-remover'




    
}
