export const ELEMENTS = {
    Usuario:  '#fmLay\\:usernameLoginZW',
    Senha: '#fmLay\\:pwdLoginZW',
    BotaoEntrar: '#fmLay\\:btnEntrar',
    BotaoModuloTreino: '#loginModuloTreinoNovo > a', 
    JanelaUsuario: '.user-section > .pct',
    BotaoLogoff: '.button-log-out'
 }