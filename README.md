<h1 align="center">Testes automatizados - Treino 3.0</h1>

<h4 align="center"> 
	🚧  Em construção... 🚧
</h4>


### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina a seguinte ferramenta:
[Node.js](https://nodejs.org/en/). Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)


## Rodando os testes


#### Clone este repositório
$ git clone <https://gitlab.com/annacorado/teste_treino.git>

#### Instale o node no projeto
$ npm install -D node


#### Instale a versão 7.0 do Cypress
$ npm install cypress@7.0.0

#### Execute os testes
$ npx cypress open

### Features

- [x] Login Treino
- [x] Login Cross
- [x] Login Avaliação
- [x] Dashboard Treino
- [x] Dashboard Avaliação
- [x] Dashboard Agenda
- [x] Dashboard Cross
- [x] Agenda de aulas
- [x] Aula com Freepass
- [x] Configuração de aulas
- [x] Montagem de Treino Renovado
- [x] Montagem de Treino Atual
- [x] Cenários para Treino Renovado
- [x] Cenários para Treino Atual
- [x] Avaliação Física
- [X] Objetivos
- [x] WOD Cross
- [x] Cadastro de Aparelhos - Treino
- [x] Cadastro de Atividades - Treino
- [x] Cadastro de Categoria de Atividades - Treino
- [x] Cadastro de Músculos - Treino
- [x] Cadastro de Grupos Musculares - Treino
- [x] Cadastro de Fichas Predefinidas - Treino
- [x] Cadastro de Níveis - Treino
- [x] Logs do sistema
- [x] Cadastro de Aparelhos - Cross
- [x] Cadastro de Atividades - Cross
- [x] Cadastro de Benchmarks - Cross
- [x] Cadastro de Tipos de Benchmarks - Cross
- [x] Cadastro de Tipos de WOD - Cross

